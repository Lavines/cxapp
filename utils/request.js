const BASE_URL = 'http://127.0.0.1:7001'; // 设置基本请求 URL

const requestInterceptor = (config) => {
    // 添加请求拦截逻辑
    let token = uni.getStorageSync("token");
    if (token == "") {
        uni.navigateTo({
            url: "../login/index",
        })
    }
    // 在这里可以对请求进行处理，例如添加请求头、签名等
    config.header = {
        ...config.header
    };
    config.header.Authorization = token
    return config;
};

const request = (config) => {
    const requestConfig = {
        ...config,
        header: requestInterceptor(config).header,
        url: BASE_URL + config.url,
    };

    return new Promise((resolve, reject) => {
        uni.request({
            ...requestConfig,
            success: ({ data }) => {
                if (data.code == 200) {
                    resolve(data);
                } else {
                    uni.showToast({
                        title: data.message ? data.message : data.data.message ? data.data.message : "系统错误",
                        icon: "none",
                        duration: 3000
                    })
                }
            },
            fail: (err) => {
                reject(err);
            },
        });
    });
};

export const get = (url, params = {}) => {
    const config = {
        url,
        method: 'GET',
        data: params,
    };

    return request(config);
};

export const post = (url, data = {}) => {
    const config = {
        url,
        method: 'POST',
        data,
    };

    return request(config);
};