import { post } from '@/utils/request.js'
export function login(params) {
    return post("/api/user/login", params)
}
export function register(params) {
    return post("/api/user/register", params)
}
export function userInfo(params) {
    return post("/api/user/info", params)
}
export function userUpdate(params) {
    return post("/api/user/update", params)
}

export function upload(params) {
    return post("/api/upload", params)
}
export function changePassword(params) {
    return post("/api/user/changePassword", params)
}