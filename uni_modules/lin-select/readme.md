# lin-select
## **使用说明**

### **==注意：需要依赖 [uni-easyinput](https://ext.dcloud.net.cn/plugin?name=uni-easyinput) ，请先导入[uni-easyinput](https://ext.dcloud.net.cn/plugin?name=uni-easyinput)后再使用；==**

```html
//将插件导入到Hubilder之后,直接通过标签使用<lin-select />
<template>
	<!-- 普通用法 -->
		<lin-select :list="productList1" value-key="name" max-height="180" placeholder="请输入商品名称" @input="input1" v-model="mytext1" />

	<!-- 远程加载数据 -->
		<lin-select :list="productList2" :loading="loading" loading-text="数据加载中" value-key="name" max-height="180" placeholder="请输入商品名称" @input="input2" v-model="mytext2" />
</template>
```

```javascript
<script>
	export default {
		data() {
			return {
			  mytext1: '',
			  mytext2: '',
			  productList1: [],
			  productList2: [],
			}
		},
		onLoad() {
			this.productList1 = [
				{
					"name": "特选痩肉"
				},
				{
					"name": "特选键子肉"
				},
				{
					"name": "特选梅肉"
				},
			]
		},
		methods: {
			input1(val) {
				console.log(val)
			},
			input2(val) {
				setTimeout(() => {
					this.productList2 = [
						{
							"name": "分割猪肉及附件"
						},
						{
							"name": "良种白条猪肉"
						},
						{
							"name": "土猪白条猪肉"
						}
					]
				}, 1000)
			}
		}
	}
</script>
```

# **Props**

| 参数           | 说明         | 类型               | 是否必填 |
| :----------- | :--------- | :--------------- | :--- |
| list         | 数据源数组      | Array            | 必填   |
| value-key    | 取值的key     | string           | 必填   |
| max-height   | 列表最大高度     | string \| number | 否    |
| @input       | 输入框input事件 | function         | 否    |
| v-model      | 绑定的字段      | -                | 否    |
| loading      | 是否正在加载   | Boolean          | 否    |
| loading-text | 远程加载中的文案   | string           | 否    |

## **联系作者**

如使用上有问题可以留言或者联系我哈，我会一直更新的；
