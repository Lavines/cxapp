import { post } from '@/utils/request.js'
export function noticeList(params) {
    return post("/api/notice/list", params)
}
export function cityList(params) {
    return post("/api/dictionary/city", params)
}
export function positionList(params) {
    return post("/api/position/list", params)
}
export function politicalStatus(params) {
    return post("/api/dictionary/politicalStatus", params)
}
export function educational(params) {
    return post("/api/dictionary/educational", params)
}
export function stand(params) {
    return post("/api/dictionary/stand", params)
}
export function schoolList(params) {
    return post("/api/dictionary/schoolList", params)
}
export function schoolOne(params) {
    return post("/api/dictionary/schoolOne", params)
}


/** @desc 文档查询*/
export const documentList = (data) => {
    return post("/api/document/list", data)
}

export const articleList = (data) => {
    return post("/api/article/list", data)
}
export const commentList = (data) => {
    return post("/api/comment/index", data)
}
export const commentCreate = (data) => {
        return post("/api/comment/create", data)
    }
    /** @desc 订单*/
export const orderCreate = (data) => {
    return post("/api/order/create", data)
}
export const orderList = (data) => {
    return post("/api/order/list", data)
}
export const count = (data) => {
    return post("/api/notice/count", data)
}